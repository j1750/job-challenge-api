#!/bin/sh -
curl -X POST -w '\n' -H 'Content-Type: application/json' -d @"${1:-place.json}" "http://${2:-localhost:3000}/place"
