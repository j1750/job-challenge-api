#!/bin/sh -

echo '{"name": "california", "latitude": 10.345216, "longitude": 14.12235}' > ./california.json
echo '{"name": "texas", "latitude": 13.345216, "longitude": -111.10095}' > ./texas.json
sh ./save-place.sh california.json localhost:3000
sh ./save-place.sh texas.json localhost:3000
sh ./get-distance.sh localhost:3000 california texas mi
