#!/bin/sh -

curl -X GET -w '\n' "http://${1:-localhost:3000}/distance?place_one=${2:-california}&place_two=${3:-texas}&unit=${4:-km}"
