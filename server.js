const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const projectRouter = require("./project.js");
const wrap_function = require("./wrapper.js");
const router = express.Router();

const app = express()
  .use(cors())
  .use(bodyParser.json())
  .use(projectRouter);

const port = process.env.PORT || 3000;
const server = app.listen(port, (err) => {
  if (err) {
    return console.log(err);
  }
  const host = server.address();
  console.log(`Node server listening on ${JSON.stringify(host)}`);
});
