# Job Challenge

This challenge consists of a simple REST API with two endpoints. One receives a location and stores it within an SQL database. The other can query the names of two saved locations and return
the distance between them in the desired unit. The required technologies for deploying this project are:

- NodeJS (with Express). The server itself is written in node, using the Express framework.
- Postgres (SQL database)
- Redis (in-memmory NO-SQL database). Used to calculate geolocations.
- Docker. Used to easily pull the required ecosystem.

# Testing

The challenge consists of developing a REST API. To test it, you simply need to query de API. You can use Postman, Curl or any HTTP client of your choice.
To help you with the testing process, I wrote two convenience scripts you can use to query the API. They're found within the ```./tests``` directory.

### Save a place
To save a place, you can use the ```save-place.sh``` script. The usage is as follows:

```cd ./tests && sh save-place.sh FILE_NAME HOST:PORT

Where:

- FILE_NAME: The filename of a .json file with the content of the place you'd like to save. All attributes of the Place object are required. The expected format is as follows:
```js
{"name": string, "latitude": number, "longitude": number}
```
- HOST: The IP address or domain name of your host. Usually, localhost.
- PORT: The port at which your host is listening. Default is 3000.

### Get distance between two saved places
To get the distance between two previously saved places (if you attempt to get the distance with at least one unknown place, a proper error is returned),
you can use the second script. Usage is as follows:

```cd ./tests && sh get-distance.sh HOST:PORT PLACE_ONE PLACE_TWO UNIT```

Where:

- HOST: The IP address or domain name of your host. Usually, localhost.
- PORT: The port at which your host is listening. Default is 3000.
- PLACE_ONE: The name of a previously saved place. Case insensitive. Required.
- PLACE_SECOND: The name of another previously saved place. Case insensitive. Required.
- UNIT: A supported distance unit. Supported units are: km, m, mi, ft. Optional parameter, default is km.

# Deployment

To deploy the project, you'll need a Node installation with NPM. The dependencies are listed in the package.json. The rest of the environment that conform the system are Redis
and Postgres, you can save installation and configuration time of those tools by using a docker installation and pulling supported images of each. The steps for deployment on 
a monolitic arquitecture as described below. For a multi-host arquitecture, you'll need to adapt the steps based on your network and resources.

1. Have a working and configured installation of Postgres 12.0 and Redis 6.2. Configure Postgres to allow remote connections if you'd like the database on a dedicated server. Same for Redis. If you're 
short on time, you can also install docker and proceed with steps 1.2 and 1.3
	1.1 Pull an appropiate Postgres image, you can use the official postgres:12 image. Run ```docker pull postgres:12```
	1.2 Pull an appropiate Redis image, you can use the official Redis:6.2 image. Run ```docker pull redis:6.2```
	1.3 Start the Postgres container, here, choose whether to configure a private network for your containers or to expose them. For simplicity, we'll expose them to the outside world. 
	Run the command: ```docker run -p 5432:5432 -t postgres:12```; Make sure you're not exposed to the internet and that your FireWall and SELINUX are properly configured.
	1.4 Start the Redis container, run the command ```sudo docker run -p 6379:6379 -t redis:6.2```; Be extremely careful with your environment when running this command, as Redis uses a passwordless
	security scheme by default. Which means security is handed over to the network level and is your responsability to allow access only to the desired users. Double check your router,
	Selinux and firewall configurations.
2. Have a working Node and NPM installation.
3. Clone the repository and cd into it.
4. Run npm install
5. Open the env.sh file, fill the connection and credential details based on your environment.
7. Run ```npm test```

That's it! The output should be the distance between California and Texas. You can now perform other GET and POST requests as desired to localhost, port 3000 using Postman or Curl.

# API
The API exposes two REST endpoints:

## Save a place
Saves a place within the database. The place must provide the name, the latitud and longitud. The name must be unique. The name is case-insensitive.
- HTTP VERB: POST
- Resource path: /place
- Body serialization language: JSON
- Body format:
```js
{"name": string, "latitude": number, "longitude": number}
```

## Get distance between two places in desired unit
Gets the distance between two places in the desired unit. Both places must exist within the database. The search is case-insensitive. The unit is optional, default is km.
- HTTP VERB: GET
- Resource path: /distance
- Body: N/A
- Query string parameters:
	- place_one: The url encoded name of a place saved within the database. Required.
	- place_two: The url encoded name of the second place saved within the database. Required.
	- unit: The url encoded name of the distance unit you'd like the distance to be represented with. Possible options are: km, m, mi, ft. Optional. Default is km.

# Database
The database is SQL and uses the Postgres engine. After you start up the Postgres container, connect to the desired database (postgres by default) and run the ```database.sql``` find in the
root of this repository to create the required schema. Given the triviality of this server, only one table is required with a single index.

# Paradigm
OOP Could've been used for developing this challenge. However, given the simplicity of the system and the structure of the API, I decided a functional paradigm would better fit the requirements
while keeping the codebase clean and simple for further improvements and maintenance.
