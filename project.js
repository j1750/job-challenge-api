const express = require("express");
const wrap_function = require("./wrapper.js");
const router = express.Router();
const { Pool } = require('pg');
const redis = require('redis')

const pool = new Pool();

const Query = async function ( sql, params ) {
   return (await pool.query(sql, params)).rows;
}

async function savePlace(req, res) {
	const {name, latitude, longitude} = req.body
	if(!name || !latitude || !longitude) return res.send({success: false, message: 'One or more parameters are missing. Expected: name, latitude, longitud.'})
	try {
		await pool.query('insert into places(name, latitude, longitude) values ($1,$2,$3)', [name.trim().toLowerCase(), latitude, longitude])
		return res.send({success: true})
	} catch(err) {
		res.send({ success: false, message: err.message ? err.message : JSON.stringify(err)})
	}
}

async function getDistance(req, res) {
	let {place_one, place_two, unit} = req.query
	try {
		// Verify all required parameters are provided.
		if(!place_one || !place_two) return res.send({success: false, message: `One or more of the url parameters is missing. Expected: place_one, place_two, unit` })
		// Provide default values if missing.
		if(!unit) unit = 'km'
		// Perform basic sanitization of input
		unit = unit.toString().trim().toLowerCase()
		place_one = place_one.toString().trim().toLowerCase()
		place_two = place_two.toString().trim().toLowerCase()
		// Verify parameter values are within domain
		if (!['m', 'km', 'ft', 'mi'].includes(unit)) return res.send({success: false, message: `Invalid unit: ${unit}. Expected: m, km, ft, mi` })
		// Attempt to fetch data from DB
		let placeOne = await pool.query('select latitude, longitude from places where name = $1', [place_one])
		let placeTwo = await pool.query('select latitude, longitude from places where name = $1', [place_two])
		// Verify the requested data exists
		if(placeOne.rows.length < 1) return res.send({success: false, message: `Place one: ${place_one} does not exist` })
		if(placeTwo.rows.length < 1) return res.send({success: false, message: `Place two: ${place_two} does not exist` })
		placeOne = { latitude: placeOne.rows[0].latitude.toString(), longitude: placeOne.rows[0].longitude.toString() }
		placeTwo = { latitude: placeTwo.rows[0].latitude.toString(), longitude: placeTwo.rows[0].longitude.toString() }
		const client = redis.createClient({
			url: `redis://@${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`
		})
		client.on('error', function(error){
			console.log(error)
		})
		await client.connect()
		await client.sendCommand(['GEOADD', 'Challenge', placeOne.longitude, placeOne.latitude, place_one, placeTwo.longitude, placeTwo.latitude, place_two])
		const distance = await client.sendCommand(['GEODIST', 'Challenge', place_one, place_two, unit])
		return res.send({distance: Number(distance), unit})
	} catch(err) {
		res.send({ success: false, message: err.message ? err.message : JSON.stringify(err)})
	}
}

router.post("/place", wrap_function(savePlace));
router.get("/distance", wrap_function(getDistance));

module.exports = router
